package tn.itss.iris.auth;

import javax.validation.constraints.Email;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tn.itss.iris.model.Role;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterRequest {
    private String username;

	private String password;

	@Email(message = "field email must be a valid email address")
	private String email;

    private Role role;

}
