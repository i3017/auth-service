package tn.itss.iris.auth;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.var;
import tn.itss.iris.config.JwtService;
import tn.itss.iris.model.User;
import tn.itss.iris.repository.UserRepository;
import tn.itss.iris.service.SequenceGenerator;
import tn.itss.iris.token.Token;
import tn.itss.iris.token.TokenRepository;
import tn.itss.iris.token.TokenType;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

        private final UserRepository repository;
        private final TokenRepository tokenRepository;

        private final PasswordEncoder passwordEncoder;
        private final JwtService jwtService;
        private final AuthenticationManager authenticationManager;
        private final SequenceGenerator sequenceGenerator;

        public AuthenticationResponse register(RegisterRequest request) {
                var user = User.builder()
                                .id(sequenceGenerator.generateSequence(User.SEQUENCE_NAME))
                                .username(request.getUsername())
                                .email(request.getEmail())
                                .password(passwordEncoder.encode(request.getPassword()))
                                .role(request.getRole())
                                .build();

                repository.save(user);

                var jwtToken = jwtService.generateToken(user);
                saveUserToken(user, jwtToken);

                return AuthenticationResponse
                                .builder()
                                .token(jwtToken)
                                .build();
        }

        public AuthenticationResponse authenticate(AuthenticationRequest request) {
                authenticationManager
                                .authenticate(new UsernamePasswordAuthenticationToken(
                                                request.getEmail(),
                                                request.getPassword()));

                var user = repository.findByEmail(request.getEmail())
                                .orElse(null);

                var jwtToken = jwtService.generateToken(user);
                saveUserToken(user, jwtToken);
                return AuthenticationResponse
                                .builder()
                                .token(jwtToken)
                                .build();
        }

        private void saveUserToken(User user, String jwtToken) {
                Token token = Token.builder()
                                .id(sequenceGenerator.generateSequence(Token.SEQUENCE_NAME))
                                .user(user)
                                .token(jwtToken)
                                .tokenType(TokenType.BEARER)
                                .expired(false)
                                .revoked(false)
                                .build();
                tokenRepository.save(token);
        }

}
