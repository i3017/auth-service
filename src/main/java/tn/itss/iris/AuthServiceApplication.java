package tn.itss.iris;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import tn.itss.iris.auth.AuthenticationService;
import tn.itss.iris.auth.RegisterRequest;
import tn.itss.iris.model.Role;

@SpringBootApplication
public class AuthServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(
			AuthenticationService service
	) {
		return args -> {
			RegisterRequest admin = RegisterRequest.builder()					
					.username("admin")
					.email("admin@gmail.com")
					.password("password")
					.role(Role.ADMIN)
					.build();
			System.out.println("Admin token: " + service.register(admin).getToken());

			RegisterRequest manager = RegisterRequest.builder()			
			.username("user")
					.email("user@mail.com")
					.password("password")
					.role(Role.USER)
					.build();
			System.out.println("User token: " + service.register(manager).getToken());

		};
	}

}
