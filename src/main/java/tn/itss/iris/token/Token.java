package tn.itss.iris.token;


import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tn.itss.iris.model.User;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document("tokens")
public class Token {

	@Transient
	public static final String SEQUENCE_NAME = "tokens_sequence";

  @Id
  public Long id;

  public String token;

  public TokenType tokenType = TokenType.BEARER;

  public boolean revoked;

  public boolean expired;

  @DBRef
  public User user;
}
