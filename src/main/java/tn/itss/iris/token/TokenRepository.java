package tn.itss.iris.token;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TokenRepository extends MongoRepository<Token, Integer> {


  Optional<Token> findByToken(String token);
}
