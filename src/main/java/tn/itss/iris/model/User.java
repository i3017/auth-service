package tn.itss.iris.model;

import java.util.Collection;
import java.util.Collections;

import javax.validation.constraints.Email;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document(collection = "users")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User implements UserDetails {

	@Transient
	public static final String SEQUENCE_NAME = "users_sequence";

	@Id
	private Long id;

    private String username;

	private String password;

	@Indexed(unique = true)
	@Email(message = "field email must be a valid email address")
	private String email;

    private Role role;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(role.name())); 
	}
  
	@Override
	public String getPassword() {
	  return password;
	}
  
	@Override
	public String getUsername() {
	  return email;
	}
  
	@Override
	public boolean isAccountNonExpired() {
	  return true;
	}
  
	@Override
	public boolean isAccountNonLocked() {
	  return true;
	}
  
	@Override
	public boolean isCredentialsNonExpired() {
	  return true;
	}
  
	@Override
	public boolean isEnabled() {
	  return true;
	}
}
